$(document).ready(function()
    		{
    			$("#partaddquestion").hide();
    			$("#partshowtest").hide();
    			$("#partaddquestion").hide();
    			$("#partaddtest").hide();
    			$("#partShowStatusAddCategory").hide();
    			var valueSelected;
    			// Day la id test duoc add
    			var idTest;
    			$('#choosecate').on('change', function (e) {
				    var optionSelected = $("option:selected", this);
				    valueSelected = this.value;
				    console.log(optionSelected+"|"+valueSelected);
				    $('.namecate').text($(optionSelected).text());
				    $('.idcate').text(valueSelected);
				    $("#partaddtest").show();
				    $("#partShowStatusAddCategory").show();
				    resetAddTestAccordingCategory();
				    showTestOfCategory();
				});
                function resetAddTestAccordingCategory()
                {
					$("#partaddquestion").hide();
					$("input[name='nametest']").val("");
				    $("input[name='countest']").val("");
                }
    			$('#addcategory').click(function(){
    			   $('.namecate').text($('input[name="namecate"]').val());
    			   console.log(""+$("#parentcate").val());
    			   $.post("/tracnghiem/index.php/createdata/addcate",
					  {
					    NAME : $("input[name='namecate']").val(),
					    IDPARENT : $("#parentcate").val(),
					  },
					  function(data,status){
					  	$('.idcate').text(data);
					  	alert("Đã thêm category thành công , hãy tạo các bài test cho category ở phía dưới."+data);
					  	$("#partaddtest").show();
				    	$("#partShowStatusAddCategory").show();
				    	resetAddTestAccordingCategory();
				    	valueSelected = data;
				    	showTestOfCategory();
					  });
    			});	
                function showTestOfCategory()
                {
                	$.getJSON("/tracnghiem/index.php/createdata/loadTestInCategory/"+valueSelected,function(result){
    					$('#showtest tbody').html("");
					     $.each(result, function(i, field){
					       //$("div").append(field + " ");
					       console.log(field);
					       var texthtml = '<tr>';
					       texthtml += '<td>' + field.ID + '</td>';
					       texthtml += '<td>' + field.TITLE + '</td>';
					       texthtml += '<td>' + field.CATEID + '</td>';
					       texthtml += '<td>' + field.COUNTSEN + '</td>';
					       texthtml += '</tr>';
					       $('#showtest tbody').append(texthtml);
					     });
					     $("#partshowtest").show();
					     $('#showtest').dataTable();
					});
                }
    			

    			$("#addtest").click(function(){
				  $.post("/tracnghiem/index.php/createdata/addTest",
				  {
				    title : $("input[name='nametest']").val(),
				    count : $("input[name='countest']").val(),
				    cateid : valueSelected
				  },
				  function(data,status){
				  	idTest = data;
				    alert("Đã thêm tên bài test thành công , bài test được thêm vào có id là:"+data+ ",Giờ bạn hãy thêm câu hỏi cho bài test ở phía dưới.");
				    $("#partaddquestion").show();
				    showTestOfCategory();
				  });
				});
    			var form = document.forms.namedItem("formaddquestion");
					  form.addEventListener('submit', function(ev) {

					  var oOutput = document.getElementById("outputaddquestion");
					  var oData = new FormData(document.forms.namedItem("formaddquestion"));

					  oData.append("TESTID", idTest);

					  var oReq = new XMLHttpRequest();
					  oReq.open("POST", "/tracnghiem/index.php/createdata/addQuestion", true);
					  oReq.onload = function(oEvent) {
					    if (oReq.status == 200) {
					      //oOutput.innerHTML = "Uploaded!"+oReq.responseText;
					      	alert("Đã thêm thành công , điền lại form để tiếp tục thêm câu hỏi");
					      	resetFormAddQuestion();
					    } else {
					        alert("Đã xảy ra lỗi , vẫn chưa thêm được");
					    }
					  };

					  oReq.send(oData);
					  ev.preventDefault();
				}, false);

		    function resetFormAddQuestion()			  	
		    {
		    	$("input[name='ANSWERA']").val("");
		    	$("input[name='ANSWERB']").val("");
		    	$("input[name='ANSWERC']").val("");
		    	$("input[name='ANSWERD']").val("");
		    	$("input[name='CORRECT']").val("");
		    	$("input[name='uploadFile']").val("");
		    }



    });