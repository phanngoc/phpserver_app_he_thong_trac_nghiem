<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>Tạo data cho app trắc nghiệm</title>
	<style type="text/css">
      .area{
      	border:1px solid black;
      	padding: 10px;
      	margin: 10px;
      }
	</style>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>


	<link rel="stylesheet" href="http://localhost/tracnghiem/public/datatable/css/jquery.dataTables.css">
	<script src="http://localhost/tracnghiem/public/datatable/js/jquery.dataTables.min.js"></script>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="http://localhost/tracnghiem/public/dist/css/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="http://localhost/tracnghiem/public/dist/css/bootstrap-theme.min.css">

	<!-- Latest compiled and minified JavaScript -->
	<script src="http://localhost/tracnghiem/public/dist/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="http://localhost/tracnghiem/public/js/viewcreatedata.js"></script>


  


</head>
<body>
  <div class="container">
   	<div class="row"> 
	   	<div class="col-md-2">
	   
		</div>

		<div class="col-md-8">
			<div class="col-md-6 area">
					        <p>Lựa chọn category đã có</p>
					       	<select id="choosecate">
					       	<?php  
					       	  foreach ($category as $key => $value) {
					       	  	echo "<option value='".$value->ID."'>".$value->NAME."</option>";
					       	  }
					       	?>	
					        </select> 
			</div>
			<div class="col-md-6 area">
					        <p> Hoặc thêm mới category ở đây:</p>
				          	<label for="namecate">Nhập tên category</label>
				        	<input type="text" name="namecate">
				        	<div>
				 			   <p>Lựa chọn parent</p>
				 			   <select id="parentcate">
				 				  <?php  
					       	        foreach ($categoryparent as $key => $value) {
					       	  	     echo "<option value='".$value->ID."'>".$value->NAME."</option>";
					       	        }
					       	      ?>	
				 			   </select>
				        	</div>
				        	<button id="addcategory">Add category</button>	
			</div>
		</div>	
	   	<div class="col-md-2"></div>
    </div> <!-- End class row -->


    <div class="row" id="partShowStatusAddCategory">
       <div class="col-md-2"></div>	
       <div class="col-md-8 area">
 		 <h4><span>Bạn đã chọn category tên:  </span><span class="namecate"></span>  <span> và id là:  </span><span class="idcate"></span></h4>
 		 <h4>Các bài test trong category này là :</h4>
 		 <div class="row" id="partshowtest">
    	 <div class="col-md-2"></div>	
         <div class="col-md-8">
         	<table id="showtest" >
         		<thead>
         			<tr>
                       <th>Id của bài test</th> 
                       <th>Name bài test</th>
                       <th>Id của loại</th>
                       <th>Số lượng câu trong bài test</th>
         			</tr>	
         		</thead>
         		<tfoot>
         			<tr>
                       <th>Id của bài test</th> 
                       <th>Name bài test</th>
                       <th>Id của loại</th>
                       <th>Số lượng câu trong bài test</th>
         			</tr>	
         		</tfoot>
         		<tbody>
                  
         		</tbody>
         	</table>
	         </div>
	         <div class="col-md-2"></div>
	         </div>	
       </div>
       <div class="col-md-2"></div>	
    </div>

    
    <div class="row" id="partaddtest">
       <div class="col-md-2"></div>	
       <div class="col-md-8 area">
       		<div>
       			<h4>Thêm bài test mà bạn muốn tạo dữ liệu</h4>
       			<p>(Hãy nhìn các bài test hiện có ở category này ở trên , bạn không tên đặt tên trùng nhé !)</p>
       			<label for="nametest">Điền tên của bài test mà bạn muốn thêm:</label>	
       			<input type="text" name="nametest"/>
       			<br/>
       			<label for="countest">Điền số lượng câu của bài test:</label>	
       			<input type="text" name="countest"/>
       			<br/>
       			<button id="addtest">Submit</button>
       		</div>
       </div>
       <div class="col-md-2">
       </div>
    </div>

    <div class="row" id="partaddquestion">
       <div class="col-md-2"></div>	
       <div class="col-md-8 area">
       		<form name="formaddquestion" enctype="multipart/form-data" method="POST">
       			<h4>Điền câu hỏi và câu trả lời vào đây.</h4>
       			<p> Upload file ảnh của câu hỏi </p>
       			<input type="file" name="uploadFile" />
       			<br/>
				    <label for="CORRECT"> Đáp án đúng :</label>
       			<input name="CORRECT" />
       			<button id="addquestion">Submit</button>
       			<p id="outputaddquestion"> </p>
       		</form>
       </div>
       <div class="col-md-2">
       </div>
    </div>

  </div> <!-- End container-->
</body>
</html>
