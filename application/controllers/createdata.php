<?php
class Createdata extends CI_Controller {
    
    function __construct()
    {
    	parent::__construct();
    }
    //
    public function show()
    {
      $result = $this->db->select('*')->from('category')->where('IDPARENT !=',0)->get()->result();
      $data['category'] = $result;
      $resultparent = $this->db->select('*')->from('category')->where('IDPARENT',0)->get()->result();
      $data['categoryparent'] = $resultparent;
      $this->load->view('viewcreatedata',$data);
    }
    public function addcate()
    {
       $data = array(
            'NAME' => $_POST['NAME'],
            'IDPARENT'  => $_POST['IDPARENT'],
       );
       $this->db->insert('category', $data);
       echo $this->db->insert_id();
    }
    public function loadTestInCategory($id)
    {
      $result = $this->db->select('*')->from('test')->where('CATEID',$id)->get()->result();
      echo json_encode($result);
    }

    public function addTest()
    {
      $data = array(
            'TITLE' => $_POST['title'],
            'CATEID'  => $_POST['cateid'],
            'COUNTSEN' => $_POST['count']
       );
       $this->db->insert('test', $data);
       echo $this->db->insert_id();
    }

    public function addQuestion()
    {
      $data = array(
            'QUESTION' => $_FILES["uploadFile"]["name"],
            'TESTID'   => $_POST['TESTID'],
            'ANSWERA'  => $_POST['ANSWERA'],
            'ANSWERB'  => $_POST['ANSWERB'],
            'ANSWERC'  => $_POST['ANSWERC'],
            'ANSWERD'  => $_POST['ANSWERD'],
            'CORRECT'  => $_POST['CORRECT'],
      );

      //$this->db->insert('sentence', $data);
    /*
      $config['upload_path'] = './public/data/question/';
      $config['allowed_types'] = 'gif|jpg|png|image/jpeg';
      $config['max_size'] = '1000';
      $config['max_width']  = '1024';
      $config['max_height']  = '1024';

    $this->load->library('upload', $config);
    print_r($_FILES);
    if ( ! $this->upload->do_upload())
    {
      echo "Da loi";
      print_r($this->upload->display_errors('<p>', '</p>'));
    }
    else
    {
      print_r($this->upload->data());
      echo "Da thanh cong";
    }
    */
      $nameava = mt_rand();
      $target_dir = "public/data/question/";
      //$target_dir = $target_dir . basename( $_FILES["uploadFile"]["name"]);
      $target_dir = $target_dir . "ques_".$nameava.".jpg";
       if (move_uploaded_file($_FILES["uploadFile"]["tmp_name"], $target_dir)) {
          echo "The file ". basename( $_FILES["uploadFile"]["name"]). " has been uploaded.";
       } 
       else
       {
          echo "Da loi";
       }  
    }

}
?>
