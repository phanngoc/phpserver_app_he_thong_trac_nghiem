<?php
class Blog extends CI_Controller {
    
    function __construct()
    {
    	parent::__construct();
    }
    //
    public function getCateByIdParent($id)
    {
	    $this->db->select('*');
		$this->db->from('category');
		$this->db->where('IDPARENT',$id);
		$query = $this->db->get();
		$result = $query->result();
		//print_r(json_encode($result));
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
    
    // Lay cac cau co ID
	public function sentence($id)
	{
	   //echo "<pre>";
	  
	    $this->db->select('*');
		$this->db->from('sentence');
		$this->db->where('ID',$id);
		$query = $this->db->get();
		$result = $query->result();
		print_r(json_encode($result));
	} 
	// lay cac bai test co category $id
	public function category($id,$userid)
	{
	    $this->db->select('*');
		$this->db->from('test');
		$this->db->where('CATEID',$id);
		$query = $this->db->get();
		$result = $query->result();
   
        foreach ($result as $key => $value) {
         $checkhave = $this->db->from('usertest')->where('USERID',$userid)->where('TESTID',$value->ID)->get()->result();
         if(count($checkhave) > 0)
         {
            //print_r($checkhave);
            $result[$key]->havedone = 1;
         }
         else
         {
            $result[$key]->havedone = 0;
         }
        }
        echo json_encode($result);
		//$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

  public function getPeopleAndMaxPointOfTest($testid)
  {
    $usertest = $this->db->from('usertest')->where('TESTID',$testid)->order_by("POINT", "desc")->limit(10)->get()->result();
    foreach ($usertest as $key => $value) {
      $usertest[$key]->user = $this->db->from('user')->where('ID',$value->USERID)->get()->result();
    }
    echo json_encode($usertest);
  }
  public function updateUserHaveDoneTest($testid,$userid)
  {
    $checkhave = $this->db->from('usertest')->where('USERID',$userid)->where('TESTID',$testid)->get()->result();

         $data = array(
            'USERID' => $userid,
            'TESTID' => $testid,
            'POINT'  => 0,
         );
         $this->db->insert('usertest', $data);
  }
	// Lay cac comment cua cau co $id
	public function comment($id)
	{
	//	echo "<pre>";
	    $this->db->select('*');
		$this->db->from('comment');
		$this->db->where('SENTENCEID',$id);
		$query = $this->db->get();
		$result = $query->result();
		foreach ($result as $key => $value) {
			$this->db->select('*');
			$this->db->from('user');
			$this->db->where('ID',$value->ID);
			$query_t = $this->db->get();
		    $result_t = $query_t->result();
		    $value->user = $result_t;
		}
		print_r(json_encode($result));
	}
	// get all question from test
	public function test($id)
	{
	//	echo "<pre>";
	  $this->db->select('*');
		$this->db->from('sentence');
		$this->db->where('TESTID',$id);
		$query = $this->db->get();
		$result = $query->result();
		//print_r(json_encode($result));
		
		foreach ($result as $key => $value) {
			$commentquery = $this->db->select('*')->from('comment')->where('SENTENCEID',$value->ID)->get()->result();
			foreach ($commentquery as $key1 => $value1) {
				$commentquery[$key1]->USERINFO = $this->db->select('*')->from('user')->where('ID',$value1->USERID)->get()->result();
			}
			$result[$key]->COMMENT = $commentquery;
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
    public function login()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $this->db->select('*');
		$this->db->from('user');
        $this->db->where('USERNAME',$username);
        $this->db->where('PASSWORD',$password);
        $result = $this->db->get()->result();
		if(!count($result))
        {
            $this->output->set_content_type('application/json')->set_output(json_encode($result));
        }
        else
        {
            $this->output->set_content_type('application/json')->set_output(json_encode($result));
        }
    }
    public function create()
	{
	   $username = $this->input->post('username');
       $password = $this->input->post('password');
       $avatar = $this->input->post('image');
       echo $avatar;
       //$type = $this->input->post('type');
       $nameava = mt_rand();
       $data = array(
            'USERNAME' => $username ,
            'PASSWORD' => $password ,
            'FULLNAME' => $username,
            'AVATAR' => "te_".$nameava.".jpg",
         );
        $this->db->select('*');
		$this->db->from('user');
		if(!count($this->db->where('username',$username)->get()->result()))
        {
            $this->db->insert('user', $data);
            $this->base64_to_jpeg($avatar, "public/data/avatar/te_$nameava.jpg");
            $data['ID'] = $this->db->insert_id();
            $this->output->set_content_type('application/json')->set_output(json_encode(array($data))); 
        }
        else
        {
            $result = array(); 
            $this->output->set_content_type('application/json')->set_output(json_encode(array($result)));
        } 
	}
    public function base64_to_jpeg( $imageData, $outputfile ) {
       /* encode & write data (binary) */
       $img = imagecreatefromstring(base64_decode($imageData));

        if($img != false)
        {
           imagejpeg($img, $outputfile);
        
        }


          //  $ifp = fopen( $outputfile, "wb" );
          //  fwrite($ifp, base64_decode($imageData)); 
          //  fclose( $ifp );
            /* return output filename */
            return( $outputfile );
       }        
    public function post_comment()
    {
       $text = $this->input->post('TEXT');
       $userid = $this->input->post('USERID');
       $sentenceid = $this->input->post('SENTENCEID');
       $data = array(
            'TEXT' => $text ,
            'USERID' => $userid ,
            'SENTENCEID' => $sentenceid,
       );
       $this->db->insert('comment', $data);
       $data['ID'] = $this->db->insert_id();
       $this->output->set_content_type('application/json')->set_output(json_encode(array($data)));
    }





    public function check()
    {
       // phpinfo();
       $imageData = "/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxQTEhUUExQWFRQWGBsbFRYYGBgVFxYYGhcYFxsaFhgYHSggGxomHhoXITEiJSksLi4uFx8zODMsNygtLiwBCgoKDg0OGxAQGzUkICY0LCwvLi8sLSwxLyw4LCwtMCwsLCwsLCwsLDEuLCwsLSwsLCwsLCwsLC8vLCwsLCwsLP/AABEIAQwAvAMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAABAUGBwgDAQL/xABKEAACAQIDBAYGBwYEBQIHAAABAgMAEQQSIQUGMUETIlFhcYEHMkKRocEUI1JicpKxCIKiwtHhM3Oy8BUkQ2PxU9IWNDVEVIOj/8QAGgEBAAMBAQEAAAAAAAAAAAAAAAMEBQIBBv/EADARAAICAQIDBQgCAwEAAAAAAAABAgMREiEEMUEFE2Fx8CIyUYGRobHRwfEzQuEj/9oADAMBAAIRAxEAPwC8aKKKAKKKKAKKKKAKK5YjEKil3ZUUalmIVQO0k6Cq+3i9MuzsPdYmbEuOUQ6l++RrAjvXNQFjUVnPa/p1x0mkEUMA7bGV/e1l/hqKYjfvauIYj6XiCT7MRKafhjtQGt6KyTh5dsN6smP8TJMvxYilBwG2iNZMSQe3E/1kqJ31rnJfVEiqsfKL+hq2vayAu6e0FIYROGHAiRMw8LNelyYvbUOokxyjnZ5WHmATRX1vlJfVB02LnF/Q1jRWXsB6X9qw9VpVky6WljW/mVyt8amuwPT2hIGMwxXh14WzDzR7EDwY1KRl2UUwbt754LHD/lsQjNzjPUkH7jWPmLin+gCiiigCiiigCiiigCiiigCiikW2dqxYWF553EcSC7MfgABqSeAA1JNAK3cAEkgAC5J0AHaTVUb7emuDDlosEoxMg06Um0KnuI1k8rDvqufSN6Up9oZooc0OE+xpnl75SOX3Rp23pl2BubJLlecmGM8BYmV/wJa9u8j31HZbGtZkzuFcpvEUIdvbw4zaEt55ZJmJ6sYvlXjoka6DTsF6ctkbg4mXWW0K/e6z/lHDzIqx9j7Fgwy2hQKebHV28WOvlwpxrIu7Tk9q1jxZqVdnxW83kjOy9xsJFqymVu2TUeSjT33qRxRhRZQFHYAAPcK+6Kzp2zseZPJfhXCG0VgKKKKjOzyvaKKAS47Z8UwtLGrj7wBPkeIqHbZ9HSG7YZ8h+w5uvk3Eed6ndeVNVxFlXushsors95FEbR2ZNhnyyo0bA9U8jbmrDQ+Rqe7kemDF4RlTEs2Kw+gIY3lQdqOfW8G49oqaYzCJKpSRFdTxDC4/se+qz3x3MOHBlgu0XtLxaPv717+XOtnhu0I2PTPZ/YyuI4KVa1R3Rp7YO3IMZCs2HkEiNzHFT9ll4q3cacqx5uZvZPs7ECaA6GwljPqSr2N2HjZuI94OrN1t4Ycfh0xEDXVuKn1kbmjjkw/oRoa0SiO9FFFAFFFFAFFFFAJto46OCJ5ZWCRxqWdjyA1P/isub9754jbGJVUVhCGth4B/rfkXtxPBRfvJmXp/3rMkqbPhYkIQ04XXNIfUj042427WXmK67tbjNh1WNGX6SyhsRLb/AAVPCNDrdzrbS2hY+yDDfb3ccpZZLTXrlh7IY93NzUhIZ7STCxJIvHEePVB9d/HQcey8vihC3tqTxY6sfE/LhTh/wWRAAEuB2G/66k0leJgSCrC3G4I91fOXWWTlmZvVQhBYieUV5XtQEwUUUUAUV8qwPChnA4kC+g8aA+qK8ooD2iiigCvCKKKArXfjc/o74jDr9XxkjHsfeX7vaOXhw4+i/fdtmYkFiThpSBOnGw5SKPtL8RcdhFqDDM2gQsDxGUkEd9VJv9uk+CkEgUiGQ9T7jcSp5947vCt3gOKc/wDznz6Mx+N4ZR9uHzRrDDYhZEV0YMjAMrDUMpFwR3WrrVEegXflgw2dO11NzhmPskamK/YRcjvBHMVe9aZnBRRRQBTVvVtkYPBz4lgD0UZYAmwZrWVb8rsQPOnWqj/aM2tkwcOHB1mlzN3pGL2/MyHyoCq9ycO2IxUmLmu5QlyTxeZycvne58ctWfglaMescxN3IJF2PE+HIdwFRncbABMLCOb3mfvv1UB8tfFalDuACTwGpr5zjbnO148je4SpQrR7i9ryoAqyNnY2XW9u837KXRbXmAAL5u8ga+4VHNmkyStIeWi91/7X99OckgUEnQCq7lKOyZZcIvoOR2sx9ZI28V/vXhx0Z4wJ5Er+lNssoVSx4AXr5wwNrt6zanu7B5f1rnXI80RHLp4f/RI8JD8xTVtHaeHY9CizZ3NsysvVF+sdRyHxtSTbO0QgKg2Nrsfsj+tMu7ctw+JYaHSIfd1t5nUnuI7Kmgnpc2vt1OWlnH8kvxWLwcCDMZlVR9w6DiTUNkxLyyLjGWVNm5+juSvS2Ogly2sEzWHnz404bI2G20ZTJN/8qjajUdO6+yP+2p0J5kEd9WYuyEliMTqOhZchW1gVtawHIVdpqUV7Sy39v+la2b5p4S+7/X5I3gsRhHGjzEjjcKCe+uWIxmDhILGch21PVyryJ7he1/G9RrF4N8BM0TEno9Vc/wDUiN8rG3OwsfvKe6um1WDKHXVH49zAWPvH6VVlDRPS0WI4nHKZMOkww9mQ+Y+RoGJw44Qk+Lf3qKbvYzQwsblB1DzZOXmOHlenmoJNxeML6HSgn8fqL5trxIL9AoF7Ek3tft04V9/8Zf2VRR3LTOyZ0ZT3qfHt/Q0ybK2oUJjk1CnzAPDy4jyPhXSc2nh8hoiSyXaMrcXPlp+lNG2dnLiIXifgw0PEq3EN5GlasCLjUHga+qi1yTznc60xxjBQsySYea2qSxPoRxVlNwQfcRWttwd4xj8DDiNM5GWUfZkXRh4HiO4is+elHZOVkxKj1upJ+IC6nzAI/dFOHoL3vOFxYwr26DFMBqbZJbWQj8Wikd69lj9Rw9qtrU/WT52+ru5uJpSiiipiIKzn+0XjS20Ior9WOAEDsZ3Yn4Ba0ZWXvTZIH21KOQES/wD81J/WgJrsmAIuUcECRjwRFH6lq4bfxNgqDix18Br8ifKl2DGjf5kn+sj5VHtqyZpz2Le3lYf+7318rFap5Z9PHZD1saO0QPaSfl8qR4jE9LKqD1Aw87cTXWefJh0A4sAPmf6edI9kW6TMeCqSfdb50S5yOhxxjZ5Uj5DrP5cB/vtrvj8WI1v7R9Ud/wDSkWym/wASVtO/4n5U1bQxEk0gWJc0shyxKeA5kt2Ko1J/tXsK3KSicykorLI9vFjGkcQLc5mUTMOWa5C37SAT4CpRsnZbYh1gU5UAu7D2E0Bt942CjzPKu28Gw0wceEiBzEmaaWQ8XkComY931jADsX3zTc7YxjjGYWkks0n3Rbqr5D4s3bWi4r2Yrp92VIyeJSl1+y9fcetk7OVVVVULGgAVRoLDgB3U8gV4igCw4CvjEzqilmNgO4k9gAA1JJsABqSatRjgpznqYxb6bA+lRZkH10VzHw6wPrRk9jWFuwhe+qmhlKXQ+ox0B0ysPZIPAjXTxHIVdKQSS6yFo1PCJGytb/uSLrm7kIA7WqG787kpZsRBEDp9dGBctzMiniW5trc8RrfNFfVqWSXh7tLw+RA0kMTBl4pqnevtJ7uHgOw1M4pgyhgRZrW8+FQgxAADUC4Kuvbpa4Gl++1j+r3uTiVzCGYKRDLYqeBjlVkXQ8AC9u7LVJ0d61v1Sz5/9Lk7e7T26ZwO4bLLbk4uPFdD8Le6o7t6Po5g/I6HwY6Hyb3BjT68BIlLPkGFaQFyLlytwigcywyE/iAGp0SbwwB0BOqm6nvDD/zXEqJ0SWtc8r6bf0ewuhbnR0x6/YjwWOaM9q81/p2Gn/DYlXF1PiOY8aiGFclRf1ho3iND7+PnSiOQqbgkHtFcTrTJk8rI9bybP6fDSxcyt1/EvWX4i3nVKYLEmKRJB6yOrDlqrAj9KujZu02Z1Vra8+B0F/Cqb2wtp5gOUjj+I1odmNrVB+Zl9pQXsy+RtHBziREccHUMPBgCP1rtUc9HOJ6TZeCYm56BASeZVcp/SpHWsZYVlH0vvbbWLI4h0PuijrV1Zk9PeyjFtRpPZxEaOPFR0ZH8IPnQErTEW6cDirBx3qwVz8c3vpjna8rEcDcj8x/rSjD4nq4ebissKq/iF1HjYt+SkcwyuPNfhe/8Pxr5uMMNn00XlZFmJlusY+yvzP8AQVxwUlkkPNjbyuR/J8a+K5weqPCvcbHXUWYjFARqlwF4sSbXP9hUr9H2yoz9dmRpZByIJii4hbcmOhbvsPZqObq7HE84zjMqWZzyJv1EHYCQT3hNeNXBhsKABmALcbkXse6rVFaKnEWtbEM3gwfT7TjjI+rhhV2/Oxt5sYvIGplgYcq35nU0zbIwZOIxkrD/ABJ1Rf8ALhhRdOwdJ0lSKrWnEslLXmKXr1kK8ZQeIvrfzHA17RXRwFFcsTiUjQvI6oii7MxCqB3k6CorifSbs1Db6QX70jkdfzBbHyr3AOe8e44djLhSI5L5ilyqFr3zIRrG19dNL9huai0rYqWVYcTYBQTI0kUWYKLAlGylSTwuumt+VqsLY+9mDxJAhxEbMeCE5X/I1mPups3w2M8hdke2aO2S18xUk5b3sA18p0Oh0tXVTVc8ybS6r4iac46Usvp4EFhxAlCrP0oSQF4ypNo5B1ZA62JY36wZ82jd16U3DQOoYPk4MODAWYHuNtD3g0pSIMyyBroeuotzZMtwewrytxpFsnRpIzwN/gbH4GqHG8bDiIrEd1h5656rxWeRe4ThZUt77b7dPBjGmjsPtAN5jqn+Wu1cMQLMh7GKnwIP8wWu9VmXF1FWzD9anjVW7cP/ADM/+bJ/rNWhs8/Wp+IVVW0pQ8sjDgzsR5sTV7s9e3J+CM7tL3Y/M1Z6Jf8A6Rg/8s/62qXVHPRzh8my8Euo+ojJv2suY/E1I61DJCqR/aVwPVwc3YZIz5hWH+lqu6oB6cdl9PsmVgLtCySjyOVv4WagKp3MJn2fkGrxO2TXn64HcDmK+Br6l6y6ceI5ajWx7OFj50wejbGFZ3jubOuYgWucvJc2gNiTr9m2nGpjtrDIpMkcgKseujKY5Aeb5D6y9rDT2uFzWRbw8u8k4+eOv0/RtcPxEe7ipeWen1/Ygje4BHA618RN1deV7+RI+VfCnKxHJtR48x8/f2Ut2Hh+kxMcZ4NIrdvVF5CD45GHnVdRzsXXLCyWXuPsnooVzCzHrv8AiYaD90ADyqUOwAuSAO06D302bRxv0aDNlzuSFRL2zyObKt+QvxPIAnlWeN5tpzYqeRppWlAchRqIxbq/Vpeyrppzta5JvWjCGmOTJnJzlsaUwarl6hDC7G4IOrMWOo7zXesv7C2nNg5RNh2KMPWUerIB7Lr7Q4+HKtI7D2ouJhWVRa4GZb3ykgNx5ggqwPMMDXXicNOPMX1xxuKWJGkc2VRc8z4AcyToBzJFdqb8bHnlRSLog6Q973yx9xt128Qhr2Ky8HjeEV36RsTIuHE8+ksjZcNDe6wC12e3Ayhfb5FgFsONTrDVo+mSJ2xWFjyk3icqoBZixdQbAak2AqJpurjCL/RMTb/Jk/TLeurm08RWyJaYx05bI6IKsjcPfOQsmExTlwxtBKxuwb/03Y6tfkTrpbsqEiCvuODrx8vrIwD9klwAw7xe/lVdS1PT8Sy6sLV8CxhHkLJa2R2UDsUMSn8JWmZmyYnxb/UP708TTZpGYixdY3I7C0Sgj3qfdTFtnSS/cD7qynHFkkXK3mCYg24MvSH7Jz/lIf5V7eu+2FzOw5MPgVpFhnuintUfpUi91Hv+x94jE9GjyfZViPHKbfG1VgBfQVNN78Tlgyji7AeQ1Pxy++mfcTZZxO0MLCPalUt+FTnb+FWrV4GGIOXxMftGebFH4GuNi4XosPDH9iNF/KoHypbRRV0zwpu3j2eMRhcRAf8AqxOmnEFkIBHfc0414aAxxuZJlxsB+8R+ZSvzq1cTI4imyjK7K6hyQbJbQINdTxN7cRxsKqto+g2kV5R4or5LLb5Va2LPUf8ACf0rM4u2dNynDm1jy8jV4GqN1ThLknn+yObRjVXypfoykbq3Ho85bKpPG110J7bHldw3Sk/57CsSFtIVcfijdQPzMtvHvr1IlVNB68LFuYNsg58utw7zTFIzQkSJ1shDAcwUYOpHcGA8NeXCJ2wstcorCzy/JaVU4VaZPL+P4Lz21AJJsMp4KzyDxVOjHwlas+rs2TpGiCM0iMysiqXa6sVbRQTxBrQ0GJSZsHOmqSHqn7skZcfFFHnUhwuBjjLmNFQyNmkKgAux0zMRxNaOjXFIy1d3b5ZMuYjZ8kZyyRvG1r5ZEaM27QGANuOtW/6IsXnwRQ+tBIY/FLCVPdnZf3aZvTBi0kxcaLYtDGQ5HJnIYKe8AA//ALBSv0PJYYs8s0Q/eCOT8GSq6embii3NaqY2NYLFr6RrEGvmiuysOSgGzaXtoeduz/fZXHaONSGN5ZDZEUsx7gL6dp7qTRykcKaN79knHQiLpWhAYMcoVgxHAMDrYHXQjUCp+9WPEgVXtb8ij5WLszsLM7M7DjZnYsQO65NOe6GyvpOMjBH1MDCbEv7KKnXRSe1mA042BqY4b0Xpf63FSuOxFSK/iesfcRUjiTC4OH6PCFjA9lbkkm12c6kse0m9UoVtPVLdmtO7vF3dS5/gh+OjKulwQxhQsDyOaTTyvbyph256y+HzqSbxyqZoyut431/C6/8AuPvqNbcOqefyrPs/zPPrYuVLEMCTFNfKfuL8NKR4UWQDsuPcSK7SN6vh8zSfDN1b97H+I10lse9SK74YjNKF+wvxOv6Wqdfs67G6TGy4lhdcPHZT2SS3UEfuCQfvVV+0cR0krv8AaY28OXwtWmvQhsP6NsuNiLPiCZW/C2ieWQA/vGtyqOiCifN3z12ORYFFFFSEQUUV4aAyJvbFbbGJUf8A5j/GU1Ym0XtG3++JAqBbWPS7bmPbjXPkspPyqZbcnCxEnxPgAW+VZXaG9kUbPZixCTPFbqp/ky/6o6b5FsF7xf4kfKlWzjeODvhce/oz8q5Y1bCP8H9/nVNbPHrqaS339ckWH6LsSJMG8J/+3msvKynLMlu4EkeC1O5pSyMpJGYEZlOVhfmDyNU/6M9rCHGtE5suJQBCeHSxEkL2Asjv+QCp1tPaM5kKRqygGwsty3fe3CtWqTcU0ZD4bvLZQ+G+/wACOy+jJi5IxhKm5u8QaS55swcBj32qY7u7CiwcIiiudczu2rSOQAWbv0AsNAABXbZCyhPrjdr6DS4HfbSlteYSexzY5Z0t5wFFFB040IgopHNtaFbgyKSPZX6x/wAiXb4V3wmKWRQyG4PcQQeYZSAVYcwQCK9GUdaiG3YYYOklkkKxjVj3k+qvNiToBxJ0r63l36ggzRxnpph7CEEKfvv6q/E9gNVjtLaEuJfpJmuV9RBcRx306o5tb2jqdeA0qKyzSXeFjYm3HYXYLHPPiJJGGVciiOO9+jTMx1++bXbyHKue15L5D3H5Vz2M1ulbsCj3Bm+dJ8Q91j7l+ZrPm3KzL9bGhCOI48/yci1Nm0ZujwpPPIB5tYfM0sxfqEfa6v5ur86Y975+qiDmSx8tB+p91WKIappeJBxM9Fcn4fkat2tkNi8VBh14yyKp7lv1m8lufKtlYaFURUUWVQFUdgAsB7qz3+zvsEyYyTFMOpAhVT/3ZNNPBM9/xDtrRNbJ84FFFFAFFFeGgMg7Dm6Taef7Usre8O1SXfTE2hYfdt+Ygfpeotu5CYtoKjcUeRTfjcK660679y6W7WA8gpP62rOvjq4mPl/LNbhZaeEm/P8ACH7YM4aHCntQr5ga/wCk122utsngR+lRbczaeiRMdVkzL3qyOpHkxB8zUq2v6qnv+X9qp3VuFuPXU0eFsVlSl65IZsXGGAB4Zh2jUdhGoqU7G35xuHAViuKjGgEpKSC3ISqDf95Se+ovKdU/F/K1KFF0PcR8bj+ldxslBbHk6IWP2kWZgvSdhW0linhP4OlXyMRJ96inD/4/wJ0SVnc6KnRTKWbkAWQAX7SbVT96dN24g0xJ4JGzdwN1S9/BmqeNzbxgqW8JGEc5JHtrFYhkeUzOgdrlUkkRRw0QA6DTzNzzqMPOWPWJbvYlz5lrk1Jd75B0cQU3Xge49h7DqPfTHLhEsQrXCm5mObIboLIAFv6wcBja/dXljaeDqmMdOcC/draRibKNF0sBoB3VNdp4KKdEmkzZEI6ZVkeNXThmlCsM4TjY36ubjpVa4H1hVnbKscM4fVShBHaCLGkGQ8VBcyv94diqJMQ+CjH0XDlVkysCFcqHYxrzjAK31JBJsLDRmQfVse8D9T/Spt6N/rXXM3UMUchXtdCMp8i9/FV7KRekLdz6G/1a/USvmTsR7EtH3Diy91x7OvV/DpVqcfn+yai5wudE35P5ZwRqFssLffc/AAH/AE1wkbh3Af1+devpFH35m/M1/nXG9U0i+uR8yaso8z5aD4kHyqHbexPSTNbUL1R5f3vT/tbHdGjMPWbqp4C9z7yfhS30MbsfTdoozreHD2lk7CwP1a+bC/eFNaHCV49pmR2jd/ovN/x+y9PRTu39B2dFGwtLJ9bKOYdwOqe9VCr5GphXgr2rxlBRRRQBRRRQGWt+MEcLt6UEWDTiQdmWazE+RZh5Ug39Gq/i/lFWX+0Ru2WSLHoP8O0U1uIUm8beAYlf3xVWbcxf0jCpL7SkCTxta/ncGq1sMWxn8i9w806LK+vP9jFs2XLLGeFmHuvY/CrNxEmeG/PS/jexqqgul6sLYmLzxgfbX+IC36/KoOOhykWuyrPeh8zm56yjxPusP5qW4EXzL2r8RwpHDkMqqzZWZWCd5uhtSmMGNxft944VTktjVjzfrojgTbjp2k8u0m2undVtYTdyPDDDQq3SB5OkEoUfWqyK7q9r2W8cZB4ZQF46tVm00yEtyNz/AF/331dO6WxBhcPGhYs4XrEknLc5siX9VATYAdlWuFaw3gzu0U/ZWfkId99gLLE8ydWRFzMPZkCjg3Y1hYN3AG4tat5MK+g1IHq9lib6frVnb14tyvQxAM8gKgE2AGXrMSATpdRw4sKZZsTChRJIZEZtFXKGuQNbFSR3620BPKvJtN4IKrHWsDbubu0J3ZpQ3RILaMUzObEC62NgtybEesvfT5vJs+eGB48P14nBALHrwHKdWYm7x9nFgdOtfTvsTawhzI0brBcsptnZGYs7hlQt1CSWDX0uQQABUjTFRyDQhlYacwwPZyIrqLi44TI5SlKWplX+i9pHlkKFQqwqtzdsoZ7gBRa5OVtSRaw0NS3e2XCNgpVmkkkdR1X1kySaBCAto1OawIFr3I504YvdHCMDkiWBtOvABE2nAMAMsi/dcMp5imbfHDGPAyqxDERjVVyAnMALLc21tperMrtMFFI5cO+uc28FYOfqob6dXUdh0pLPJlUm9u88B3nuHHypQP8ACH3Xb3N1h+vwqObz4mwVBzuT4cB86zqoa54Ni+3uqnIasfO08oVASNEjUAknWwsOZJrVHo03TXZ2CSIgdM/XnYc5COF+xR1R4X51WXoK3Bzsu0cQvVUn6Mh9phoZT3DUL3i/IVfFbEUksI+anJyk5MKKKK9OQooooAooooBJtXZ8eIhkhlXNHIpVxwuCLaHkew8qypvhu1LsvFPh5LtDIPq3tpIl+qw5Z1PEfIitbVHt+N0odpYYwy9Vh1opALtG/aO0HgRzHYbEeNZPYycXlGZNzdn/AEpp8Hp0k0RbDm3GeH6wAHlmjEqeLik272OKMYm0uerfQhhy/wB8xSjbmxMXsjGJ0gyyRsHhkGqOFNwVPMdo462NSPf/AHb+kQptjBIegnGfEotr4eYGzmw1ylr68jc8DXM4KccMkptdU1NEd3ovZGGhDHhyJF9PdS3YW8IlAixB63svwv3Hsbv5/q1HF9PAyn/EUX/FbmO+16lO5O60W1sC8MZSLHYVi0bHQTQvrlktqcr3GbWwZRwtVeNKlDRLmi/bxTruVtfKS9LzFmJQMojfXMCAeVwOB8Rf3GpVsH0jtFGIsXGzNGLCVLHOANM6kghu8aHjpVYiefDu2CxgaJ1sEZvWjbipv7SXtY/KnPZmKTF2jciOXOsb92ZwhcfdF791Ve7nVldPW5ddtHEpN+viv58fkXrsHA5/+ak1aVVMa8RHGRmAPIub3JHcNcoJdXwoPEV1giCKqKLKoCqOwAWA91dKrvd5MjW+YjGEA4ACoxtLByYW7woZYCbtCCA8ZJuTDmIBW/sEi3I26tTJqQYo0T0ktcm2RGHfqFdCMQD9lsNOSO66ofgbVFd597fp9oolZYbgszDKXym4st7qt7HXUkDQDi/7Zxaxuyr1mzAADgCxAGc8FFyBr7jVTbZxRw+HEfCV7q3aoXqt8RbzqWEpWeykXa9Ec2T6bi1NqJK80acEC2PJspKsR3C4HlTNszZ/03akOHIuryojcjkBu9j+EMaRnBy4GaEzLl6WJJAO2KUaE9/d2ipf6EsL0m2w2h6NZn94Men56v1UKue3LBT4ji3dSk+ef69eBpPDQLGiogCooCqo4AAWAHlXWvBXtWTPCiiigCiiigCiiigCiiigG/bmxIMXEYcTEssZ5NxB7VI1Vu8EGo7uVuP/AMObERJJ0uDm6yxSAFo39Vhe1nVlsDe3qjjcmplRQFC+kf0QSRO2J2apZOLYdfXjPMxX9Zfu8RyuNBWOwdtT7PxazR3SWMkMjArmB9ZHXsPZy0PECtiYidUUu7BVUXZmIAAHMk8BWZ/TJvnBj8QqYZE6OLjPkUSTNw0e2boxrYX1ve2goMnX0kekDDbVjREwbriFIySlgWAPrJZVuynlrx17abdyt1MT06YiSNQsTB+jm0Mtj6pWxKg2OpHkakm4UX0bBrlX62Ul2ZhbICAFUX1Y2F+zrc+FTbA4TKgvqx6zE6kse3ysPKmE+Z6hTsjb8eR+jlMEylyMHONMt+qsWtyLWsY2Kgv6vKpPPjpY1vJDfUA9HIrcSAPXCcz31X20MJHiJeidQyxjM3aHNsoHYQDm/eXsrui4tF6NMU7R3BCy/WkZWDDruC9rgaZrWqnPg8vMTtSJ1Lj5AVHQEFyQMzqBcKW1yZuQNR7bG0URnXF4hYwFDCNGMeZWzcSD0jEFG9XKLHUVH8VjcTPI0UuJb6vI6iMCI3IYXDJZu0ceDUyTYTI5zC5JuxOpcdpJ1PnXMeBb954O42YOmO2sGjMUKZEBYByLEqWJUheOYC2ra3F7Gq93v2a5cSLmfNfNxYhuN/A6nTTSpxLhejdo+S+r3ofV92q+Kmk2JhJU29Yar4jUe/h51drohWvZR5OyU+ZBd6d7cRjxD9IyFoFKK6rkZlJBs4HV0tpYDiasD9m7DA4zEyfZgCj9+QH+SmXbWAjkhkcIpboyytlGbhca8afP2bcRbF4pPtQhvyuB/PXTWCJrBoKiiivDwKKKKAKKKKAKKKKAKKKKAKKKKAZd8N3Y8fhJMNJoHHVYcUcaqw8D7xcVlDG4PEbOxZSRAs0TcCMysORF+KkcDWyaiO/+4OH2nGBJ9XMgIjmUXK88rD2kvy5a2tc0BWu5m24saygEK4GZ4yddPs/aF+zzqY7TxYiTNxYmyDtY/IC5PcDVM7xejfaWAfN0TyKpuk2HzOBbUE5eshHeB415h/SHOLDEL0zIMqm+QjXXMLG7GwufuigLW2BD67E3J4k8SSSxPvp1aOqhwnpSmj0WCLKTc3Lk8hxBH6VaUm0mjAaaMiMi4mivLGBoQXAAdNNb5So5tXjmlszpRk1lDDPKVxbtyyqLdwLg074/BCRNOPFT/vkaYGkDyI6kMrq9mBBBN1YWI46Zqetj48FSgzOVNhkBe3PKxGin8RHEV1qxzCy9kI9s4a8aygdZB1hzye15i2byPbTSUpy3x3kfAxpI0KnO2UI0gD6AkmyBlsNPaPEVWUm+0vqxRIgv1V6z5QeCjhoOA7rUU090eyTi8MluNwtlcrYXVsw9k6G5tyPf770o/Zuw18ZiZPsQhfzuD/JUbw2zNtYu6Jh5rEa3iEK2OnryAD41cfoa3Km2bBN9JCCWZ1NlbNlRV0BI0vctwvRvJyyxKKKK8PAooooAooooAooooAooooAooooAooooArjLhUbVkVj3qD+tdqKAyr6Z9mdBtbEAKFSQJIgGgsyANw++Hq2N3to9PsXDya5uhMRvxLJeH4lQfOo7+0psrXC4kDiGic+HXQX85K89EH1+zhD0jKy4xVUAgrlIWY3Fr+xJztUdkNawSVT0PJY826mDZ85w6Zr3JAIzG1rsAbNpprelsuFRENlAsNByHlXfocQOKRv3q5S/flKm3hmNMG+W1JcNhZJnEUaxrm6zGQk8EUKAoJLWHrc6hdUn0J42xT5lJ+mHa/TY0QqbrAoW3/cbrN/KP3TV8ejXdNcBgokZF6cjNM9hmzvqVzWvZRZf3apv0LbtHH498XOM0cDdIxI0ediWUdhsbsR+HtrSNWIR0xSK9k9cnIKKKK6OAooooAooooAooooAooooAooooAooooAooooAooooCC+mvZnTbJnNrtFllXuysA38Jaq1/ZzV2xU4/wCkiK5/zOtGv8Lye4VZHpt2j0OyZwOMpSIfvNc/wq1UbuJvKuDDZJXhlY9ZhbKwHAEHQ214jma8k8LJ1COp4yasdrC9Zz9MO9L47FrgcNd1SQKQpv0s56oUdoW+Xxv3Ui3r9JmKlQxR4qQg+sVyx/FFB+NSH0AbnF3O0Jl6qXXD39p+Dyd4Hqg9pbsr2Mso8lHDxktfcDdldn4KLDixcDNKw9qRtWPgNFHcoqR0UUPAooooAooooAooooAooooAooooAooooAooooAooooAooooBJtTZsWIjaKeNZY29ZGFwbajzHbVb470FbPd8ySYiJT7CsrAeBdSfeTVp0UBWWy/Qfs6M3kM0/c7hV90YU/GrGwODSGNY41CRoAqKBYKBwArvRQBRRRQBRRRQBRRRQBRRRQBRRRQH//Z";
       $img = imagecreatefromstring(base64_decode($imageData));

        if($img != false)
        {
           imagejpeg($img, "public/test.jpg");
        
        }
    }
    public function check1()
    {
      echo str_replace("world","Peter","Hello world worldsadasdaf world!");

    }
    
    
}
?>
